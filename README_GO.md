#### GOLANG / HOW-TO

1. Create a `Dockerfile` file 
```
$ touch Dockerfile
```

2. Copy following code into your `Dockerfile`
```
# use Golang latest docker image
FROM golang:latest

# create a directory in docker container
RUN mkdir /app

# add all files & dirs into /app folder in docker container
COPY . /app/

RUN go get -u -v github.com/gin-gonic/gin

# change work directory into /app
WORKDIR /app

# build go app
RUN go build -o main .

# Execute binary
CMD ["/app/main"]
```

3. Create a `docker-compose.yml` file
```
$ touch docker-compose.yml
```

4. Copy following code into your `docker-compose.yml` file
```
version: '3'

services:
  go: 
    build: .
    ports: 
      - 8080:8080
```

5. Now, before intergrating with Gitlab pipeline, make sure your project is working on docker by executing command below
```
$ docker-compose up
```

You should see the application log and says that it is working, and you need to try to hit the api endpoint, for this project, please execute this command

```
$ curl -i http://localhost:8080/ping
```

You should receive following response

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Date: Mon, 24 Dec 2018 08:08:00 GMT
Content-Length: 18

{"message":"pong"}%
```