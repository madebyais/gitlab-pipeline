# gitlab-pipeline

#### REQUIREMENTS

Before you get started, please make sure that you already installed following requirements:

- Docker engine
- Docker compose

#### HOW-TO

Please read `README_GO.md` if you are working on a Golang project.

