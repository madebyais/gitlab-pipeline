# use Golang latest docker image
FROM golang:latest

# create a directory in docker container
RUN mkdir /app

# add all files & dirs into /app folder in docker container
COPY . /app/

RUN go get -u -v github.com/gin-gonic/gin

# change work directory into /app
WORKDIR /app

# build go app
RUN go build -o main .

EXPOSE 8080

# Execute binary
CMD ["/app/main"]